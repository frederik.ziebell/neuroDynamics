\documentclass[a4paper,10pt]{scrartcl}
\input{input/packages.tex}
\input{input/definitions.tex}

			
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ARTICLE SETUP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Revealing age-related changes of adult hippocampal neurogenesis using mathematical models}
\author[1,2]{Frederik Ziebell}
\author[2]{Sascha Dehler}
\author[2,*]{Ana Martin-Villalba}
\author[1,3,*]{Anna Marciniak-Czochra}
\affil[1]{{\small Institute of Applied Mathematics, Heidelberg University}}
\affil[2]{{\small German Cancer Research Center (DKFZ), Heidelberg}}
\affil[3]{{\small Interdisciplinary Center of Scientific Computing (IWR) and BIOQUANT, Heidelberg University}}
\affil[*]{{\small Corresponding author, Shared last authorship}}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ARTICLE START
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%					
\begin{document}
\setcounter{section}{0}
\renewcommand{\thesection}{S\arabic{section}}
\setcounter{table}{0}
\renewcommand{\thetable}{S\arabic{table}}
\setcounter{figure}{0}
\renewcommand{\thefigure}{S\arabic{figure}}
\section*{Supplementary Information}


\section{Astrocytic Transformation}
We want to give an explanation for the claim that NSC apoptosis is almost non-detectable in the case of $1-\theta=60.9\%$ of the NSC decline resulting from apoptosis. Consider the stem cell model (2.1) including aging effects (2.2). To model the dynamics of NSC apoptosis, we add a new compartment $c_\mathrm{A}$ accounting for \textit{apoptotic}, i.e.\ biologically dead but physically present stem cells. The dynamics of NSCs at a certain age $\tau$ is thus given by
\[
\begin{aligned}
\ddt c_0(t) &=-(r+q(\tau)) c_0(t) + 2 a p c_1(t),\\
\ddt c_1(t) &= r c_0(t) - p c_1(t),\\
\ddt c_\mathrm{A}(t) &= (1-\theta)q(\tau) c_0(t) - \delta_\text{phag} c_\mathrm{A}(t),
\end{aligned}
\]
where $\delta_\text{phag}$ is the rate at which apoptotic cells are cleared via phagocytosis within, on average, $1/\delta_\text{phag}=\SI{1.5}{\hour}$ \autocite{Sierra:2010}. At the age of $\tau = 2$\;months, there are about $n_0=10000$ NSCs in the entire dentate gyrus \autocite{Encinas:2011} and the number of apoptotic NSCs can be calculated by that number times the steady state fraction of apoptotic NSCs on all NSCs. Accordingly, there are
\[
n_0\cdot\lim_{t\to\infty}\frac{c_\mathrm{A}(t)}{c_0(t)+c_1(t)+c_\mathrm{A}(t)} = 9.3
\]
apoptotic stem cells in the entire dentate gyrus. Considering the usual sampling fraction of one sixth of the DG, there only remain about two apoptotic stem cells to be detected.

To analyze the scenario that the accumulation of astrocytes could be explained with a higher transformation rate $\theta$ if in addition astrocytes are allowed under go apoptosis, we consider the modified dynamics 
\begin{equation}
\ddt c_2(t) = \theta q(t) c_0(t) - d_2c_2(t),
\end{equation}
where $d_2$ is the death rate of astrocytes. Estimating $\theta$ and $d_2$ simultaneously yields $\theta=0.392$ and $d_2=\SI{2.6e-6}{\per\day}$, showing that there is no justification for such scenario. In addition, assuming $\theta=1$ and only estimating $d_2$ results in an AICc score of $178.9$, which compared to the AICc of $144.7$ for the no-apoptosis model further indicates that there is no support for this scenario from a model selection viewpoint.

\section{Dynamics of Progenitor Cells}\label{sec:prog}
To model the dynamics of progenitor cells, we again make use of the study of \textcite{Encinas:2011}. We consider the  data set in which the authors label dividing cells with BrdU and track the number of BrdU labeled progenitors (Appendix 1 Fig. \ref{afig:ProgDynamics}). Because of the rapid increase and subsequent decrease of labeled cells, they concluded that progenitors perform a series of symmetric self-renewing divisions, followed by subsequent transformation into neuroblasts.
\begin{figure}	
	\centering
	\includegraphics[height=150pt]{include/progBrduData.pdf}
	\captionof{figure}{Time course of the dynamics of neural progenitor cells. Two months old mice were injected with BrdU and sacrificed at several time points after injection. Depicted is the number of BrdU positive progenitor cells. Data is reproduced from the publication of \textcite{Encinas:2011}.}
	\label{afig:ProgDynamics}
\end{figure}

We model the dynamics of progenitors with the equations
\begin{equation}\label{eq:ProgModel}
\begin{aligned}
\ddt P_k(t) &= 2pP_{k+1}(t)-pP_k(t),\\
\ddt P_N(t) &= -p P_N(t)
\end{aligned}
\end{equation}
for $k=0,\ldots,N-1$. Here, $P_i$ is the number of progenitor cells with $i$ remaining divisions and $p>0$ is the proliferation rate. Moreover, we assume that at the start of the experiment, all progenitors have $N$ remaining divisions, i.e.\ $P_N(0)=n$ for some $n>0$ and $P_k(0)=0$ for $k\neq N$. 

For quantifying $p$, we consider the corresponding cell cycle length $t_c$, which is linked to $p$ via (4.1). The cell cycle length of progenitor cells has been measured in different studies, however with contrasting results ranging from $12-\SI{14}{\hour}$ to about $\SI{22}{\hour}$ \autocite{Hayes:2002,Farioli:2014}. We thus employ an unbiased approach for quantification by assuming different cell cycle lengths $t_c$ and compute the $R^2$ of the fit dependent on $N$, the maximum number of progenitor divisions (Appendix 1 Fig. \ref{afig:progR2}).
\begin{figure}
	\centering
	\includegraphics[height=150pt]{include/tcR2.pdf}
	\captionof{figure}{Goodness of fit of the progenitor model \eqref{eq:ProgModel} to the data displayed in Appendix 1 Fig. \ref{afig:ProgDynamics}. The $R^2$ is calculated solely from the first five time points of the data, which capture the rapid rise and fall of progenitor numbers.}
	\label{afig:progR2}
\end{figure}

The best fit can be obtained for $N=2$, $3$ or $4$, but only $N=2$ allows for a cell cycle length in the range of what is experimentally observed with the maximum $R^2$ at $t_c=\SI{14.4}{\hour}$. To achieve a better compromise between our model assumption and the measured cell cycle lengths, we relax the condition of an optimal $R^2$. A visual assessment of the fit shows that $R^2=0.95$ provides a reasonable fit to the data. For $N=2$, the maximal $t_c$ for which $R^2=0.95$ can be achieved is $t_c=\SI{15.6}{\hour}$ (Appendix 1 Fig. \ref{afig:progFit}), which we assume for our subsequent analysis.
\begin{figure}	
	\centering
	\includegraphics[height=150pt]{include/n2tc16.pdf}
	\captionof{figure}{Fit of the progenitor model \eqref{eq:ProgModel} to the data of Appendix 1 Fig. \ref{afig:ProgDynamics}, assuming $N=2$ progenitor divisions and a cell cycle length of $t_c=\SI{15.6}{\hour}$.}
	\label{afig:progFit}
\end{figure}

\end{document}